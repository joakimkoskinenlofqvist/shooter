package game.world 
{
	import game.entities.Enemy;
	import game.entities.Player;
	import net.flashpunk.World;
	
	public class GameWorld extends World
	{
		public var wave:int = 0;
		public var player:Player;
		public var enemies:Vector.<Enemy> = new Vector.<Enemy>();
		
		public function GameWorld() 
		{
			trace("Vi är i GameWorld");
			
			Globals.gameWorld = this;
			
			player = new Player();
			add(player);
		}
		
		public function addEnemy(enemy:Enemy):void
		{
			//lägg till fienden till vår värld
			add(enemy);
			
			//lägg till fienden till vår array
			enemies.push(enemy);
		}
		
		public function removeEnemy(enemy:Enemy):void
		{
			//ta bort fienden från vår värld
			remove(enemy);
			
			//ta bort fienden från vår array
			for (var i:int = enemies.length - 1; i >= 0; i--) {
				if (enemies[i] == enemy) {
					enemies.splice(i, 1);
					break;
				}
			}
		}
		
		override public function update():void 
		{
			if (enemies.length <= 0) {
			wave += 5;
			trace("Round: " + wave / 5 + "	enemies: " + wave);

				
			for (var i:int = 0; i < wave; i++) {
				addEnemy(new Enemy());
				
				
			}
			}
			super.update();
		}
		
	}

}