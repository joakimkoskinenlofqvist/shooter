package game.entities 
{
	import game.world.GameWorld;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	public class Enemy extends Entity
	{
		public var alive:Boolean = true;
		
		private var _health:Number = 1;
		
		public var speed:Number = 3;
		
		public var vx:Number = 0;
		public var vy:Number = 0;
		
		public function Enemy() 
		{
			super(0, 0);
			setSpawnPosition();
			
			graphic = Image.createRect(20, 20, 0x0000FF);
			graphic.x = -10;
			graphic.y = -10;
			
			setHitbox(20, 20, 10);
			
			type = "enemy";
			
			setSpawnPosition();
			
			// Kollar så vi inte krockar i någon annan enemy
			// så att vi fastnar.
			// ifall vi gör de så lägg ny position
			
			while (collide("enemy", x, y)) {
				setSpawnPosition();
			}
			
		}
		override public function update():void 
		{
			
			vx = 0;
			vy = 0;
			
			if (Globals.gameWorld.player.x > x) {
				vx += speed;
			}
			if (Globals.gameWorld.player.x < x) {
				vx -= speed;
			}
			
			if (Globals.gameWorld.player.y > y) {
				vy += speed;
			}
			if (Globals.gameWorld.player.y < y) {
				vy -= speed;
			}
			
			moveBy(vx, vy, "enemy");
		}
		
		
		public function get health():Number
		{
			return _health;
		}
		
		public function set health(value:Number):void
		{
			_health = value;
			
			if (_health <= 0) {
				die();
			}
		}
		
		public function die():void
		{
			if (alive) {
			alive = false;
			Globals.gameWorld.removeEnemy(this);
			}
		}
		
		public function setSpawnPosition():void
		{
			x = -20;
			y = 480 * Math.random();
			var r:Number = Math.random();
			
			if (r < 0.25) {
				x = 640 + 20;
			} else if (r < 0.5) {
				y = -20;
				x = 640 + 20 * Math.random();
			} else if (r < 0.75) {
				y = 480 + 20;
				x = 640 * Math.random();
			}
		}
		private function tooCloseToEnemy():Boolean
		{
			for (var i:int = Globals.gameWorld.enemies.length - 1; i >= 0; i--)
			{
				if (FP.distance(x, y,
				Globals.gameWorld.enemies[i].x,
				Globals.gameWorld.enemies[i].y) < 20)
				{
					return true;
				}
			}
			return false;
		}
		
	}

}
//fråga jimi om hur man gör så man låser upp 1,2,3,4 efter X antal rounds