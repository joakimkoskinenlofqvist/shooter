package game.entities.weapons 
{
	import game.entities.Bullet;
	import net.flashpunk.Entity;
	
	public class WeaponShotgun extends Weapon
	{
		
		public function WeaponShotgun(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			cooldown = 0;
		}
		
		override public function shoot(DirX:Number, DirY:Number):void
		{
			// lägg till skote till världen
			for (var i:int = 0; i < 20; i++) {
			var b:Bullet = new Bullet(this, owner.x, owner.y, 
			DirX - 0.2 + 0.4 * Math.random(), DirY - 0.2 + 0.4 * Math.random());
			
			b.damage = 1
			
			owner.world.add(b);
			}
			super.shoot(DirX, DirY);
			
		}
	}

}