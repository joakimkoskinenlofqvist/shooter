package game.entities.weapons 
{
	import game.entities.Bullet;
	import net.flashpunk.Entity;
	
	public class WeaponMachineGun extends Weapon
	{
		
		public function WeaponMachineGun(_owner:Entity, _evHandleShooting:Function = null) 
		{
			super(_owner, _evHandleShooting);
			cooldown = 0;
		}
		
		override public function shoot(DirX:Number, DirY:Number):void
		{
			// lägg till skote till världen
			
			owner.world.add(new Bullet(this, owner.x, owner.y, 
			DirX - 0.1 + 0.1 * Math.random(), DirY - 0.1 + 0.1 * Math.random()));
			
			super.shoot(DirX, DirY);
			

		}
	}

}