package game.entities 
{
	import game.entities.weapons.Weapon;
	import net.flashpunk.graphics.Image;
	
	public class BulletGranade extends Bullet
	{
		
		public function BulletGranade(_weapon:Weapon, _x:int, _y:int, _DirX:Number, _DirY:Number) 
		{
			super(_weapon, _x, _y, _DirX, _DirY);
			
			graphic = Image.createRect(14, 14, 0x000000);
			graphic.x -= 7;
			graphic.y -= 7;
			
			setHitbox(14, 14, 7, 7);
			
			speed = 10;
		}
		
		override public function remove():void 
		{
			for (var i:int = 0; i < 100; i++) {
				/*
				// ruti spridning 
				var b:Bullet = new Bullet(weapon, x, y,
					-1 + 2 * Math.random(), -1 + 2 * Math.random());
				*/
					// cirkel spridning
				var a:Number = Math.PI * 2 * Math.random();
				var b:Bullet = new Bullet(weapon, x, y,
					Math.cos(a), Math.sin(a));
					
				b.speed = 2 * 4 * Math.random();
				
				weapon.owner.world.add(b);
				
			}
			
			
			super.remove();
		}
		
	}

}